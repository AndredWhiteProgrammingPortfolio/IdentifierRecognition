//
//  Andre White
//  Awhiteis21@csu.fullerton.edu
//  
//  Created by Andre White on 2/22/16.
//  Copyright © 2016 AndreWhite. All rights reserved.
//

#include <iostream>
#include <string.h>
#include <stdio.h>
using namespace std;
char tokens[]="abcdefghijklmnopqrstuvwxyz_1234567890";         //array with all possible identifier tokens

bool isIdentifier(char* input){
    bool found=false;
    for(int x=0 ; x<27 ;x++){                                  //the first character in an identifier should be a letter
        if(input[0]==tokens[x]){                               //if the first character matches any letter than continue
            found=true;
            break;
        }
    }
    if(found){
        for(int x=1;x<strlen(input);x++){                      //iterate through all input to make sure they match
            if(found){
                found=false;
                for(int y=0;y<37;y++){                         //iterate through all tokens to find a match
                    if(input[x]==tokens[y]){
                        found=true;
                        break;
                    }
                }
            }
        }
    }
    return found;                                               //if all characters are found then TRUE, else FALSE
}
char prompt(){
    char response;                                              //user Response
    char* input;                                                //user input
    cout<<"Enter A String:";
    cin.getline(input, 256);
    if(isIdentifier(input)){                                    //inform user if input is identifier or not
        cout<<input<<" is Identifier"<<endl;
    }
    else{
        cout<<input<<" is not Identifier"<<endl;
    }
    cout<<"CONTINUE(y/n)?";
    cin>>response;
    cin.ignore();
    return response;
}
int main(int argc, char* argv[]){
    while(prompt()=='y'){                                     //Run loop
    }
    return 0;
}
